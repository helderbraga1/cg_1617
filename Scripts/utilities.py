import bpy
from math import*

# 000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
# B O O L E A N - M O D I F I E R
# ===============================
#
# Make the boolenan "operation" between the objects "objA" and "objB"
#         - Operations: 'DIFFERENCE' / 'INTERSECT'  /  'UNION'
#         - Results:     objA-objB   / objA^objB    /  objA+objB
#         - Side-effect 1: the object "objA" will be result of the operation
#         - Side-effect 2: the object "objB" will be destroyes
def boolean_modifier(objA, objB, operation):
    # Free all objects from selection
    bpy.ops.object.select_all(action='DESELECT')
    # Select the basic object "objA"
    bpy.data.objects[objA.name].select = True
    # Prepare the boolean modifier
    bpy.ops.object.modifier_add(type='BOOLEAN')
    bpy.context.object.modifiers["Boolean"].object = bpy.data.objects[objB.name]
    bpy.context.object.modifiers["Boolean"].operation = operation
    bpy.ops.object.modifier_apply(apply_as='DATA', modifier="Boolean")
    # Take the result object and save it in "objA"
    objA = bpy.context.scene.objects.active
    # Delete the desnecesary object "objB"
    bpy.ops.object.select_all(action='DESELECT')
    bpy.data.objects[objB.name].select = True
    bpy.ops.object.delete(use_global=False)
    return objA


##################################################
###               GERAL
##################################################

# Junta os objetos tornando-os num so
def join_objects(objects):
    # Deselect all object
    bpy.ops.object.select_all(action='DESELECT')
    # Select all the objects of the list "objects"
    for i in range(0, len(objects)):
        #print(i)
        bpy.data.objects[objects[i].name].select = True
    # Join all objects
    bpy.ops.object.join()
    # Catch the juncton object under the name "obj" and pass it
    obj = bpy.context.scene.objects.active
    return obj

# Parenta os objetos
def parent_objects(parent_object, objects):

    # Deselect all object
    bpy.ops.object.select_all(action='DESELECT')

    # Select all the objects of the list "objects"
    for obj in objects:
        obj.select = True

    # Activating the Parent
    bpy.context.scene.objects.active = parent_object

    bpy.ops.object.parent_set(type='OBJECT', xmirror=False, keep_transform=True)  # set parent

    return bpy.context.scene.objects.active

# Duplica o objeto
def duplicate(ob):
    # Free all objects from selection
   # bpy.ops.object.select_all(action='DESELECT')

    # Create new mesh
    mesh = bpy.data.meshes.new("Nameiii")

    # Create new object associated with the mesh
    ob_new = bpy.data.objects.new("Nameiii", mesh)

    # Link new object to the given scene and select it
    bpy.context.scene.objects.link(ob_new)

    # Copy data block from the old object into the new object
    ob_new.data = ob.data.copy()
    ob_new.scale = ob.scale
    ob_new.location = ob.location

    ob_new.select = True

    return ob_new

# Elimina todos os objecto que contenham mesh na cena
def delete_all():
    # select objects by type
    for o in bpy.data.objects:
        if o.type == 'MESH':
            o.select = True
        else:
            o.select = False

    # call the operator once
    bpy.ops.object.delete()

# Criacao de uma camera
def add_cam(loc, rot):
    bpy.ops.object.camera_add(location=loc, rotation=rot)
    return bpy.context.active_object

# Criacao de um objeto vazio
def add_empty(loc):
    bpy.ops.object.empty_add(location=loc)
    return bpy.context.active_object


##################################################
###               ILUMINACAO
##################################################

# Cria uma lampada --> type:POINT/SPOT/SUN/HEMI/AREA
def create_light(type, energy=1, color=(1, 1, 1), shadow_method='RAY_SHADOW'):
    scene = bpy.context.scene

    # Create new lamp datablock
    lamp_data = bpy.data.lamps.new(name="New Lamp", type=type)
    lamp_data.energy = energy
    lamp_data.color = color
    lamp_data.shadow_method = shadow_method

    # Create new object with our lamp datablock
    lamp_object = bpy.data.objects.new(name="New Lamp", object_data=lamp_data)

    # Link lamp object to the scene so it'll appear in this scene
    scene.objects.link(lamp_object)

    # And finally select it make active
    lamp_object.select = True
    scene.objects.active = lamp_object

    return lamp_object
