#Ultima modificação: 28/03/2017 20:03
#Ultima edição por: H.F.M.M.B.
#Autores: Pedro Moniz
#         Hélder Filipe Mendonça de Medeiros Braga
#Versão: 1.0.3                 Major.Medium.Minor    Modifications



import bpy # Importa a livraria bpy
from math import atan, sqrt # Importa as funções "atan" e "sqrt" da livraria math

def delete_all(): # Função para apaga todos os objecto que contenham uma mesh na scene
    # Selecionar objectos por tipo
    for o in bpy.data.objects:
        if o.type == 'MESH': #Verifica se o objecto seleccionado e do tipo mesh ou não
            o.select = True # É do tipo mesh
        else:
            o.select = False #Não é do tipo mesh

    # call the operator once
    bpy.ops.object.delete() 
    
#    Join all objects inside the list "objects" to one object and pass it as object 
def sum(objects):
   # Deselect all object
   bpy.ops.object.select_all(action='DESELECT')
   # Select all the objects of the list "objects"
   for i in range(0,len(objects)):
      bpy.data.objects[objects[i].name].select = True
   # Join all objects
   bpy.ops.object.join()
   # Catch the juncton object under the name "obj" and pass it 
   obj=bpy.context.scene.objects.active   
   return obj

# Dois tuplos-SIZE(size_x,size_y,size_z) || LOCATION(x,y,z)
def cube(size, location):
    
    # Create one long Cube
    bpy.ops.mesh.primitive_cube_add(radius = 1)
    bar = bpy.context.scene.objects.active
    bar.scale = (size[0],size[1],size[2])
    bar.location = (location[0], location[1], location[2] + bar.scale.z)
    return bar

def plataforma(main_size=(0.4,0.4,20), distance_legs = [5, 5], floors_height = 0.35, floors = 10):
    
    objects = []
    
    positions = [(distance_legs[0], distance_legs[1],0),(distance_legs[0],-distance_legs[1],0),(-distance_legs[0],distance_legs[1],0),(-distance_legs[0],-distance_legs[1],0)]
    
    # Main Legs
    for position in positions:
        c = cube(location=position, size = main_size)
        objects.append(c)
        
        
    # X floors ao longo do comprimento da plataforma
    
    #floors_height = 0.35
    floors_distance = ((main_size[2] * 2) - floors_height *2) / floors
    
    for i in range(floors + 1):
        c = cube((distance_legs[0],distance_legs[1],floors_height), (0,0,floors_distance *i))
        objects.append(c)
        
    # Creating the cross between floors
    pos = [(distance_legs[0],0,0),(-distance_legs[0],0,0),(0,distance_legs[1],0),(0,-distance_legs[1],0)]
    comp_x = sqrt(distance_legs[0] ** 2 + floors_height ** 2)
    comp_y = sqrt(distance_legs[1] ** 2 + floors_height ** 2)
    
    index = 0
    middle_size = 0.085
    for p in pos:    
        for i in range(floors):
            c_pos = (p[0],p[1],p[2] + (floors_height / 2) + i * floors_distance + (floors_distance/2 ))
            
            if index < 2:
                s = (middle_size,comp_y,middle_size)
                c = cube(size=s, location=c_pos)
                c_2 = cube(size=s, location=c_pos)
                objects.append(c)
                objects.append(c_2)
                c.rotation_euler = (atan((floors_distance/2) / (distance_legs[1]+main_size[0])) ,0,0)
                c_2.rotation_euler = (-atan((floors_distance/2) / (distance_legs[1]+main_size[0])) ,0,0)
            else:    
                s = (comp_x,middle_size,middle_size)
                c = cube(size=s, location=c_pos)
                c_2 = cube(size=s, location=c_pos)
                objects.append(c)
                objects.append(c_2)
                c.rotation_euler = (0, atan((floors_distance/2) / (distance_legs[0]+main_size[0])),0)
                c_2.rotation_euler = (0, -atan((floors_distance/2) / (distance_legs[0]+main_size[0])),0)
                            
        index += 1
        
    return sum(objects)

def estrutura():
    #Variaveis
    #Torre_lançamento
	#Inteiros
    torre_lancamento_01_comprimento = int(6)
    torre_lancamento_01_largura = int(5)
    torre_lancamento_01_antena_superior_01_posicao_z = int(60)
    torre_lancamento_01_barras_suporte_lateral_altura = int(25)
	#Floats
    torre_lancamento_01_barras_suporte_lateral_comprimento = float(0.25)
    torre_lancamento_01_barras_suporte_lateral_largura = float(0.25)
    
	
	#Inicio Templates Variaveis, feel free to delete
    largura = float()
    largura = float()
    largura = float()
    largura = float()
    largura = float()
    largura = float()
    largura = float()
    largura = float()
    largura = float()
    largura = float()
    largura = float()
    largura = float()
    largura = float()
    largura = float()
    largura = float()
    largura = float()
    largura = float()
    largura = float()
    posicao_z = int()
    posicao_z = int()
    posicao_z = int()
    posicao_z = int()
    posicao_z = int()
    posicao_z = int()
    posicao_z = int()
    posicao_z = int()
    posicao_z = int()
    posicao_z = int()
    posicao_z = int()
    posicao_z = int()
    posicao_z = int()
    posicao_z = int()
    posicao_z = int()
    posicao_z = int()
    posicao_z = int()
	#Fim Templates Variaveis
    
	# Selecionar cycles render engine
    cena = bpy.context.scene
    if not cena.render.engine == 'CYCLES':
        cena.render.engine = 'CYCLES'
	
    torre_lancamento_01 = plataforma(distance_legs = [torre_lancamento_01_comprimento, torre_lancamento_01_largura], main_size = (torre_lancamento_01_barras_suporte_lateral_comprimento,torre_lancamento_01_barras_suporte_lateral_largura,torre_lancamento_01_barras_suporte_lateral_altura))
    edificio_auxiliar_torre_lancamento_01 = plataforma(distance_legs = [25, 10], main_size = (0.25,0.25,15), floors = 5)
    edificio_auxiliar_torre_lancamento_01.location.x = 25 + 6.5
    
    torre_lancamento_01_antena_intermedia_01 =plataforma(distance_legs = [2, 2], main_size = (0.25,0.25,2.5), floors = 2)
    torre_lancamento_01_antena_intermedia_01.location.z = 53
    
    # Antena 
    bpy.ops.mesh.primitive_cylinder_add(depth = 11, radius = 0.15) #Cria um novo mesh do tipo cilindrico
    torre_lancamento_01_antena_superior_01 = bpy.context.scene.objects.active #Coloca o foco no objeto
    torre_lancamento_01_antena_superior_01.location.z = torre_lancamento_01_antena_superior_01_posicao_z #Define a posicao da antena no espaço no eixo z
	
	#INICIO ZONA TESTES#######################################
    

#body
    bpy.ops.mesh.primitive_cube_add()
    bpy.ops.transform.resize(value=(1.2, 0.8, 1.5))

#head
    bpy.ops.mesh.primitive_monkey_add()
    bpy.ops.transform.translate(value=(0, -0.3, 2))

#arms
    bpy.ops.mesh.primitive_cylinder_add()
    bpy.ops.transform.translate(value=(2, 0, 0))
    bpy.ops.transform.resize(value=(0.5, 0.5, 1.5))
    bpy.ops.transform.rotate(value=-0.523599, axis=(0, 1, 0))
    bpy.ops.mesh.primitive_cylinder_add()
    bpy.ops.transform.translate(value=(-2, 0, 0))
    bpy.ops.transform.resize(value=(0.5, 0.5, 1.5))
    bpy.ops.transform.rotate(value=0.523599, axis=(0, 1, 0))

#legs
    bpy.ops.mesh.primitive_cylinder_add()
    bpy.ops.transform.translate(value=(0.7, 0, -3.5))
    bpy.ops.transform.resize(value=(0.5, 0.5, 2))
    bpy.ops.object.duplicate_move(TRANSFORM_OT_translate={"value":(-1.4, 0, 0)})
	
	
	
	
	
	
	
	
import bpy

bpy.ops.mesh.primitive_cube_add(radius=1, enter_editmode=False, location=(0.0, 0.0, 0.0))

bpy.ops.object.mode_set( mode   = 'EDIT'   )
bpy.ops.mesh.select_mode( type  = 'FACE'   )
bpy.ops.mesh.select_all( action = 'SELECT' )

bpy.ops.mesh.extrude_region_move(
    TRANSFORM_OT_translate={"value":(0, 0, 2)}
)

bpy.ops.object.mode_set( mode = 'OBJECT' )


bpy.ops.mesh.primitive_cylinder_add(radius=1, enter_editmode=False, location=(3.0, 0.0, 0.0))

i= int(2)
for i in range(4):
    bpy.ops.object.mode_set( mode   = 'EDIT'   )
    bpy.ops.mesh.select_mode( type  = 'FACE'   )
    bpy.ops.mesh.select_all( action = 'SELECT' )

    bpy.ops.mesh.extrude_region_move(TRANSFORM_OT_translate={"value":(0, 0, i)})
    bpy.ops.object.mode_set(mode = 'OBJECT')

import bpy
import math


def delete_all(): # Função para apaga todos os objecto que contenham uma mesh na scene
    # Selecionar objectos por tipo
    for o in bpy.data.objects:
        if o.type == 'MESH': #Verifica se o objecto seleccionado e do tipo mesh ou não
            o.select = True # É do tipo mesh
        else:
            o.select = False #Não é do tipo mesh

    # call the operator once
    bpy.ops.object.delete() 
      
delete_all()#Chama a função que apaga todos os objectos do tipo mesh existentes na cena    


bpy.ops.mesh.primitive_cube_add(radius=1, enter_editmode=False, location=(0.0, 0.0, 0.0))

bpy.ops.object.mode_set( mode   = 'EDIT'   )
bpy.ops.mesh.select_mode( type  = 'FACE'   )
bpy.ops.mesh.select_all( action = 'SELECT' )

bpy.ops.mesh.extrude_region_move(
    TRANSFORM_OT_translate={"value":(0, 0, 2)}
)

bpy.ops.object.mode_set( mode = 'OBJECT' )


bpy.ops.mesh.primitive_cylinder_add(radius=1, enter_editmode=False, location=(3.0, 0.0, 0.0))

i= int(5)
for i in range(0,6,2):
    bpy.ops.object.mode_set( mode   = 'EDIT'   )
    bpy.ops.mesh.select_mode( type  = 'FACE'   )
    bpy.ops.mesh.select_all( action = 'SELECT' )

    bpy.ops.mesh.extrude_region_move(TRANSFORM_OT_translate={"value":(0, 0, i)})
bpy.ops.object.mode_set(mode = 'OBJECT')



import bpy
from math import radians

# Add the base cube shape
bpy.ops.mesh.primitive_cube_add()

# Create a reference to the active object (which is the cube since we just added it)
cube = bpy.data.objects[ bpy.context.object.name ]

# Add a new subdivision surface modifier and set its view and render subdivision levels to 4
subsurf1 = cube.modifiers.new( "Subsurf", "SUBSURF" )
subsurf1.levels = subsurf1.render_levels = 4

# Add a new displacement modifier, set its texture to a new noise texture, and set the displacement strength to 0.15
disp = cube.modifiers.new( "Disp", "DISPLACE" )
disp.texture = bpy.data.textures.new( "DisplaceNoise", "NOISE" )
disp.strength = 0.15

# Add a 2nd subdivision surface modifier, and set its view and render levels to 2
subsurf2 = cube.modifiers.new( "Subsurf", "SUBSURF" )
subsurf2.levels = subsurf1.render_levels = 2

# Add a camera and set its location and rotation values
bpy.ops.object.camera_add( location = (0, -4, 0 ), rotation = (radians(90),0,0) )
camera = bpy.data.objects[ bpy.context.object.name ]
bpy.context.scene.camera = camera

# Give our rock object a new material
cube.active_material = bpy.data.materials.new("Rock.Mat")
mat = cube.active_material # Create a shorter reference variable to the material

# Set material properties
mat.diffuse_intensity  = 0.45
mat.specular_intensity = 0.1
mat.use_shadeless      = True

# Create a new musgrave texture and apply it to our materials as its active texture
mat.active_texture = bpy.data.textures.new( "Diffuse", "MUSGRAVE" )
mat.texture_slots[0].color = (0,0,0)         # Set base texture color to black
mat.texture_slots[0].texture_coords = 'ORCO' # Set texture coordiantes to object

# Set render filepath and render
bpy.context.scene.render.filepath = "/tmp/rock.png"
bpy.ops.render.render( write_still = True )







	
	#FIM ZONA TESTES##################################################
	
	
	
	
	
	

delete_all()#Chama a função que apaga todos os objectos do tipo mesh existentes na cena          
         
estrutura()#Chama a função que cria as estruturas