import bpy


##################################################
###               GERAL
##################################################

# Cria um material
def makeMaterial(name, diffuse_color=(1, 1, 1), specular_color=(1, 1, 1), alpha = 1):
    mat = bpy.data.materials.new(name)
    mat.diffuse_color = diffuse_color
    mat.diffuse_shader = 'LAMBERT'
    mat.diffuse_intensity = 1.0
    mat.specular_color = specular_color
    mat.specular_shader = 'COOKTORR'
    mat.specular_intensity = 0.5
    mat.alpha = alpha
    mat.ambient = 1
    return mat

# Aplica um material a um objeto
def setMaterial(ob, mat):

    # Free all objects from selection
    bpy.ops.object.select_all(action='DESELECT')

    me = ob.data

    me.materials.append(mat)

# This textures are going to be create in blender (materials will not !)
def applyTexture(material, texture_name, scale=(1, 1, 1), offset=(1, 1, 1)):

    # Aplica a textura a uma material
    texture = bpy.data.textures.get(texture_name)
    mtex = material.texture_slots.add()
    mtex.texture = texture
    mtex.scale = scale
    mtex.offset = offset

    return mtex


##################################################
###               MATERIAIS
##################################################

# Cria um o material do simbolo da nasa
def material_simbolo_nasa(material_name="Simbolo Nasa"):

    # Criar o Material
    material = makeMaterial(material_name)

    # Colocar textura
    texture = applyTexture(material, "simbolo_nasa", scale=(3.5, 5.4, 3.5), offset=(1.0, 0.0, 0.0))
    texture.texture_coords = 'UV'
    texture.mapping = "FLAT"

    return material

# Criar o material do tanque
def material_tanque():

    material = makeMaterial(name="Tanque Principal", diffuse_color=(0.559, 0.191, 0.046))
    material.specular_intensity = 0.15

    return material

# Criar o material do propulsor
def material_propulsor():

    # Criar o material
    material = makeMaterial(name="Tanque Pequeno", diffuse_color=(0.8, 0.8, 0.8))
    material.specular_intensity = 1

    # Efeito de espelho
    material.raytrace_mirror.use = True
    material.raytrace_mirror.reflect_factor = 0.25

    return material

# Criar o material do detalhe da asa
def material_asa_detalhe():

    # Criar o material
    material = makeMaterial(name="Asa detalhe", diffuse_color=(0.05, 0.05, 0.05))
    material.specular_intensity = 1
    # Efeito espelho
    material.raytrace_mirror.use = True
    material.raytrace_mirror.reflect_factor = 0.15

    return material

# Criacao do material do planeta Terra
def material_planeta_terra():

    # Criar o Material
    material = makeMaterial("Planeta Terra")
    material.specular_intensity = 0.275

    # Colocar textura
    texture = applyTexture(material, "planeta_terra")

    # Aplicar o normal map para dar relevo ao planeta
    normal_map = applyTexture(material, "normal")
    normal_map.use_map_normal = True
    normal_map.normal_factor = 0.55
    normal_map.use_map_color_diffuse = False



    return material

# Criacao do material das nuvens do planeta Terra
def material_planeta_terra_nuvens():

    # Criar o Material
    material = makeMaterial("Planeta Terra")
    material.use_transparency = True
    material.alpha = 0
    material.specular_alpha = 0

    # Colocar textura
    texture = applyTexture(material, "clouds")
    texture.use_map_alpha = True
    texture.alpha_factor = 1.0
    # Uso do normap map para dar relevo
    texture.use_map_normal = True
    texture.normal_factor = 3.0

# Criacao do material do planeta Marte
def material_marte():

    # Criar o Material
    material = makeMaterial("Planeta Marte")
    material.specular_intensity = 0.275

    # Colocar textura
    texture = applyTexture(material, "marte_difusse")

    # Uso do normap map para dar relevo
    normal_map = applyTexture(material, "marte_normal")
    normal_map.use_map_color_diffuse = False
    normal_map.use_map_normal = True
    normal_map.normal_factor = 0.55

    return material

# Criacao do material do chao(cena_1)
def material_chao():

    material = makeMaterial("Chao")
    material.specular_intensity = 0.0

    # Colocar textura
    texture = applyTexture(material, "chao_terra")
    texture.scale = (35, 35, 35)

    return material

# Criacao do material da estrutura de lancamento
def material_estrutura_lancamento():
    material = makeMaterial(name="Estrutura Lancamento", diffuse_color=(0.182, 0.196, 0.24))
    return material

# Criacao do material da base de lancamento
def material_base_lancamento():
    material = makeMaterial(name="Base Lancamento", diffuse_color=(0.1, 0.1, 0.1))
    return material



