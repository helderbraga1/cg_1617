import bpy
from math import atan, sqrt, radians
from utilities import*
from materials import*
from mathutils import Vector

##################################################
###               ESTRUTURAS
##################################################

# Cricao de um objeto tipo cubo.Dois tuples-SIZE(size_x,size_y,size_z) || LOCATION(x,y,z)
def cube(size=(1, 1, 1), location=(0, 0, 0)):
    # Create one long Cube
    bpy.ops.mesh.primitive_cube_add(radius=1)
    c = bpy.context.scene.objects.active
    c.scale = (size[0], size[1], size[2])
    c.location = (location[0], location[1], location[2] + c.scale.z)
    return c

# Criacao de um objeto tipo cilindro
def cylinder(size = (1, 1, 1), location = (0, 0, 0)):
    bpy.ops.mesh.primitive_cylinder_add(radius=1)
    cy = bpy.context.scene.objects.active
    cy.scale = (size[0], size[1], size[2])
    cy.location = (location[0], location[1], location[2] + cy.scale.z)
    return cy

# Criacao de um objeto com parametros utilizado na criacao de uma infrastrutura para o lancamento e cenario
def plataforma(main_size=(0.4, 0.4, 20), distance_legs=[5, 5], floors_height=0.35, floors=10, bars_radius = 0.085):

    objects = []

    # Posicao das pernas
    positions = [(distance_legs[0], distance_legs[1], 0), (distance_legs[0], -distance_legs[1], 0),
                 (-distance_legs[0], distance_legs[1], 0), (-distance_legs[0], -distance_legs[1], 0)]

    # Main Legs
    for position in positions:
        c = cube(location=position, size=main_size)
        objects.append(c)

    # X floors ao longo do comprimento da plataforma
    floors_distance = ((main_size[2] * 2) - floors_height * 2) / floors

    for i in range(floors + 1):
        c = cube((distance_legs[0], distance_legs[1], floors_height), (0, 0, floors_distance * i))
        objects.append(c)

    # Criacao dos objetos cruzados na lateral
    pos = [(distance_legs[0], 0, 0), (-distance_legs[0], 0, 0), (0, distance_legs[1], 0), (0, -distance_legs[1], 0)]
    comp_x = sqrt(distance_legs[0] ** 2 + floors_height ** 2)
    comp_y = sqrt(distance_legs[1] ** 2 + floors_height ** 2)

    index = 0
    for p in pos:
        for i in range(floors):
            c_pos = (p[0], p[1], p[2] + (floors_height / 2) + i * floors_distance + (floors_distance / 2))

            if index < 2:
                s = (bars_radius, comp_y, bars_radius)
                c = cylinder(size=s, location=c_pos)
                c_2 = cylinder(size=s, location=c_pos)
                objects.append(c)
                objects.append(c_2)
                c.rotation_euler = (atan((floors_distance / 2) / (distance_legs[1] + main_size[0])), 0, 0)
                c_2.rotation_euler = (-atan((floors_distance / 2) / (distance_legs[1] + main_size[0])), 0, 0)
            else:
                s = (comp_x, bars_radius, bars_radius)
                c = cylinder(size=s, location=c_pos)
                c_2 = cylinder(size=s, location=c_pos)
                objects.append(c)
                objects.append(c_2)
                c.rotation_euler = (0, atan((floors_distance / 2) / (distance_legs[0] + main_size[0])), 0)
                c_2.rotation_euler = (0, -atan((floors_distance / 2) / (distance_legs[0] + main_size[0])), 0)

        index += 1

    # Retornar os objetos todos juntos
    return join_objects(objects)

# Criacao de uma infrastrutura de lancamento para o foguetao utilizando a funcao "plataforma
def estrutura():
    p1 = plataforma(distance_legs=[6, 5], main_size=(0.25, 0.25, 25))
    p2 = plataforma(distance_legs=[25, 10], main_size=(0.25, 0.25, 15), floors=5)
    p2.location.x = 25 + 6.5

    p3 = plataforma(distance_legs=[2, 2], main_size=(0.25, 0.25, 2.5), floors=2)
    p3.location.z = 53

    p4 = plataforma(distance_legs=[10, 8], main_size=(0.5, 0.5, 5), floors=2, bars_radius=0.2)
    p4.location.x = -20


    # Antena
    bpy.ops.mesh.primitive_cylinder_add(depth=20, radius=0.15)
    antena = bpy.context.scene.objects.active
    antena.location.z = 60

    all_objs = join_objects([p1, p2, p3, p4, antena])
    all_objs.name = "Estrutura Lançamento"

    return all_objs

# VARIAVEL GLOBAL #
# Utilizada nas funcoes abaixo ate a funcao "edificio_pesquisa_operacoes"
tamanho_multiplier = 20.00

# Criaca do edificio principal
def edificio_principal():

    objs = []

    # EDIFICIO PRINCIPAL
    # Andar 0 Centro
    edificio_principal_tras = cube((0.35 * tamanho_multiplier, 3.5 * tamanho_multiplier, 0.4 * tamanho_multiplier),
                                   (4.15 * tamanho_multiplier, -3.8 * tamanho_multiplier, 0.0 * tamanho_multiplier))

    objs.append(edificio_principal_tras)

    # Andar 0 Esquerda
    edificio_principal_esquerda = cube((4.5 * tamanho_multiplier, 0.35 * tamanho_multiplier, 0.4 * tamanho_multiplier),
                                       (0.0 * tamanho_multiplier, -7.65 * tamanho_multiplier, 0.0 * tamanho_multiplier))

    objs.append(edificio_principal_esquerda)

    # Andar 1 abaixo dos pilares
    edificio_principal_esquerda_andar_01 = cube(
        (1.15 * tamanho_multiplier, 0.35 * tamanho_multiplier, 0.4 * tamanho_multiplier),
        (0.0 * tamanho_multiplier, -7.65 * tamanho_multiplier, 0.8 * tamanho_multiplier))

    objs.append(edificio_principal_esquerda_andar_01)

    # Andar 0
    edificio_principal_frente = cube((0.35 * tamanho_multiplier, 3.5 * tamanho_multiplier, 0.4 * tamanho_multiplier),
                                     (-4.15 * tamanho_multiplier, -3.8 * tamanho_multiplier, 0.0 * tamanho_multiplier))

    objs.append(edificio_principal_frente)


    # Pilares Telhado Tras
    edificio_principal_esquerda_andar_02_01 = cylinder(
        (0.12 * tamanho_multiplier, 0.12 * tamanho_multiplier, 0.37 * tamanho_multiplier),
        (1.03 * tamanho_multiplier, -7.88 * tamanho_multiplier, 1.6 * tamanho_multiplier))
    objs.append(edificio_principal_esquerda_andar_02_01)

    edificio_principal_esquerda_andar_02_02 = cylinder(
        (0.12 * tamanho_multiplier, 0.12 * tamanho_multiplier, 0.37 * tamanho_multiplier),
        (-1.03 * tamanho_multiplier, -7.88 * tamanho_multiplier, 1.6 * tamanho_multiplier))
    objs.append(edificio_principal_esquerda_andar_02_02)


    edificio_principal_esquerda_andar_02_03 = cylinder(
        (0.12 * tamanho_multiplier, 0.12 * tamanho_multiplier, 0.37 * tamanho_multiplier),
        (0.0 * tamanho_multiplier, -7.88 * tamanho_multiplier, 1.6 * tamanho_multiplier))

    objs.append(edificio_principal_esquerda_andar_02_03)


    # Pilares Telhado Frente
    edificio_principal_esquerda_andar_02_04 = cylinder(
        (0.12 * tamanho_multiplier, 0.12 * tamanho_multiplier, 0.3 * tamanho_multiplier),
        (1.03 * tamanho_multiplier, -7.42 * tamanho_multiplier, 1.6 * tamanho_multiplier))

    objs.append(edificio_principal_esquerda_andar_02_04)


    edificio_principal_esquerda_andar_02_05 = cylinder(
        (0.12 * tamanho_multiplier, 0.12 * tamanho_multiplier, 0.3 * tamanho_multiplier),
        (-1.03 * tamanho_multiplier, -7.42 * tamanho_multiplier, 1.6 * tamanho_multiplier))
    objs.append(edificio_principal_esquerda_andar_02_05)


    edificio_principal_esquerda_andar_02_06 = cylinder(
        (0.12 * tamanho_multiplier, 0.12 * tamanho_multiplier, 0.3 * tamanho_multiplier),
        (0.0 * tamanho_multiplier, -7.42 * tamanho_multiplier, 1.6 * tamanho_multiplier))
    objs.append(edificio_principal_esquerda_andar_02_06)



    # Porta
    edificio_principal_esquerda_andar_00_01 = cube(
        (0.07 * tamanho_multiplier, 0.07 * tamanho_multiplier, 0.3 * tamanho_multiplier),
        (0.35 * tamanho_multiplier, -7.225 * tamanho_multiplier, 0.0 * tamanho_multiplier))
    objs.append(edificio_principal_esquerda_andar_00_01)


    edificio_principal_esquerda_andar_00_02 = cube(
        (0.07 * tamanho_multiplier, 0.07 * tamanho_multiplier, 0.3 * tamanho_multiplier),
        (-0.35 * tamanho_multiplier, -7.225 * tamanho_multiplier, 0.0 * tamanho_multiplier))
    objs.append(edificio_principal_esquerda_andar_00_02)


    edificio_principal_esquerda_andar_00_03 = cube(
        (0.43 * tamanho_multiplier, 0.07 * tamanho_multiplier, 0.1 * tamanho_multiplier),
        (0.0 * tamanho_multiplier, -7.225 * tamanho_multiplier, 0.5 * tamanho_multiplier))
    objs.append(edificio_principal_esquerda_andar_00_03)


    # Telhado
    edificio_principal_telhado = cube(
        (1.20 * tamanho_multiplier, 0.38 * tamanho_multiplier, 0.042 * tamanho_multiplier),
        (0.0 * tamanho_multiplier, -7.65 * tamanho_multiplier, 2.229 * tamanho_multiplier))
    edificio_principal_telhado.rotation_euler = (-0.25, 0, 0)
    objs.append(edificio_principal_telhado)

    return join_objects(objs)

# Criacao do parque de estacionamento
def parque_estacionamento():
    # PARQUE DE ESTACIONAMENTO EDIFICIO PRINCIPAL
    posx = -3.0 * tamanho_multiplier
    posy = -6.4 * tamanho_multiplier
    while (posy * tamanho_multiplier < 1 * tamanho_multiplier):
        while (posx * tamanho_multiplier < 3.6 * tamanho_multiplier):
            parque_estacionamento_edificio_principal = cube(
                (0.20 * tamanho_multiplier, 0.30 * tamanho_multiplier, 0.001 * tamanho_multiplier), (posx, posy, 0.0))
            posx = posx + 0.6 * tamanho_multiplier
        posx = -3.0 * tamanho_multiplier
        posy = posy + 0.8 * tamanho_multiplier

# Criacao da base de lancamento do foguetao
def base_lancamento_foguetao():
    # BASE LANCAMENTO FOGUETAO
    return cube((2.5 * tamanho_multiplier, 1.5 * tamanho_multiplier, 0.005 * tamanho_multiplier),
                                    (-6.2 * tamanho_multiplier, 5.5 * tamanho_multiplier, 0.0 * tamanho_multiplier))

# Criacao da base do chao
def base_chao():
    # BASE LANCAMENTO FOGUETAO
    base_chao = cube((10.0 * tamanho_multiplier, 10.0 * tamanho_multiplier, 0.0001 * tamanho_multiplier),
                     (0.0 * tamanho_multiplier, 0.0 * tamanho_multiplier, 0.0 * tamanho_multiplier))

# Criacao do edificio de pesquisa de operacoes
def edificio_pesquisa_operacoes():
    # EDIFICIO PESQUISA E OPERAÇOES
    posz = 0.0
    while (posz < 5):
        edificio_pesquisa_operacoes = cube(
            (1.00 * tamanho_multiplier, 1.30 * tamanho_multiplier, 0.001 * tamanho_multiplier),
            (5.5 * tamanho_multiplier, 5.5 * tamanho_multiplier, posz * tamanho_multiplier))
        posz = (posz + 0.25)
    compx = 1.00
    compy = 1.30
    while (compx > 0 and compy > 0):
        compx = (compx - 0.025)
        compy = (compy - 0.025)
        posz = (posz + 0.007)
        edificio_pesquisa_operacoes = cube(
            (compx * tamanho_multiplier, compy * tamanho_multiplier, 0.06 * tamanho_multiplier),
            (5.5 * tamanho_multiplier, 5.5 * tamanho_multiplier, posz * tamanho_multiplier))
    edificio_pesquisa_operacoes_parede_01 = cube(
        (2.5075 * tamanho_multiplier, 1.30 * tamanho_multiplier, 0.022 * tamanho_multiplier),
        (6.5 * tamanho_multiplier, 5.5 * tamanho_multiplier, 2.48 * tamanho_multiplier))
    edificio_pesquisa_operacoes_parede_01.rotation_euler = (0, 1.562, 0)

    edificio_pesquisa_operacoes_parede_02 = cube(
        (2.5075 * tamanho_multiplier, 1.025 * tamanho_multiplier, 0.022 * tamanho_multiplier),
        (5.4972 * tamanho_multiplier, 6.79 * tamanho_multiplier, 2.48 * tamanho_multiplier))
    edificio_pesquisa_operacoes_parede_02.rotation_euler = (1.562, 1.562, 0)

##################################################
###               FOGUETAO
##################################################

# Criacao de uma shape com formato cilindrico que numa das bases é pontiagudo.(Usado para a criacao do foguetao e do
# tanque principal(maior)
def shape_foguetao():

    # Altura do objeto
    depth = 20
    # Raio do objeto
    radius = 2.85

    # Criar um circulo
    bpy.ops.mesh.primitive_circle_add(radius=radius)
    main_part = bpy.context.scene.objects.active

    # Edit Mode
    bpy.ops.object.editmode_toggle()

    # Criar a parte cilindrica do objeto(reta)
    bpy.ops.mesh.extrude_region_move(TRANSFORM_OT_translate={"value": (0, 0, depth)})

    cupula_height = 7
    extrusions = 8
    increment = cupula_height / extrusions

    value = 1

    # Criacao da parte em bico
    for i in range(extrusions + 1):
        value = 1 - 0.0155 * i ** 2
        increment = 1.18 - 0.0155 * i ** 2

        bpy.ops.mesh.extrude_region_move(TRANSFORM_OT_translate={"value": (0, 0, increment)})
        bpy.ops.transform.resize(value=(value, value, value))

    # Object Mode
    bpy.ops.object.editmode_toggle()
    bpy.ops.object.shade_smooth()
    main_part.scale = (1, 1, 1.3)

    # Retornar o objeto criado
    return main_part

# Criacao individual do propulsor(para ser chamado na "propulsores")
def propulsor():

    # Raio do tanque
    radius = 2.3

    bpy.ops.mesh.primitive_circle_add(radius=radius, vertices=60)
    obj = bpy.context.scene.objects.active

    # Edit Mode
    bpy.ops.object.editmode_toggle()

    # Creating the piece
    bpy.ops.mesh.extrude_region_move(TRANSFORM_OT_translate={"value": (0, 0, 1.5)})
    bpy.ops.transform.resize(value=(0.85, 0.85, 0.85))

    # Creating the piece
    bpy.ops.mesh.extrude_region_move(TRANSFORM_OT_translate={"value": (0, 0, 5)})
    bpy.ops.transform.resize(value=(0.9, 0.9, 0.9))

    # Cricao da parte principal do corpo(sem ponta)
    bpy.ops.mesh.extrude_region_move(TRANSFORM_OT_translate={"value": (0, 0, 3)})

    for i in range(12):

        bpy.ops.mesh.extrude_region_move(TRANSFORM_OT_translate={"value": (0, 0, 0.05)})

        if i % 2:
            bpy.ops.transform.resize(value=(1.05, 1.05, 1.05))  # Experimenta so 1

        else:
            bpy.ops.transform.resize(value=(0.9, 0.9, 0.9))

        bpy.ops.mesh.extrude_region_move(TRANSFORM_OT_translate={"value": (0, 0, 2)})

    # Criacao da pontado propulsor
    bpy.ops.mesh.extrude_region_move(TRANSFORM_OT_translate={"value": (0, 0, 2)})
    bpy.ops.transform.resize(value=(0.0, 0.0, 0.0))

    # Object Mode
    bpy.ops.object.editmode_toggle()

    return obj

# Criacao dos dois propulsores do foguetao
def propulsores():
    # Criacao e posicao do objeto
    s_1 = propulsor()
    s_1.location = (4, 0, -6)
    s_2 = propulsor()
    s_2.location = (-4, 0, -6)

    # Acessorios (pequenos cilindros para dar detalhe)
    positions_x = [4, -4]
    positions_z = [13.85, 17.8, 28]
    scale = [1.65, 1.5, 1.4]
    objects = []
    for i in range(len(positions_x)):
        for j in range(len(positions_z)):
            # Criacao do objeto
            a_1 = cylinder((scale[j], scale[j], 0.1))
            # Posicionamento
            a_1.location = (positions_x[i], 0, positions_z[j])

            # Criacao do material do detalhe
            material_a = material_asa_detalhe()
            # Aplicacao do material
            setMaterial(a_1, material_a)
            objects.append(a_1)

    # Juntar os objetos criados
    obj_together = join_objects(objects)

    # Criacao do material
    material = material_propulsor()
    # Aplicacao do material
    setMaterial(s_1, material)
    setMaterial(s_2, material)

    # Retorno dos objetos todos juntos
    return join_objects([s_1, s_2, obj_together])

# Aviao do foguetao
def aviao():

    # Parte 1 do aviao
    aviao = shape_foguetao()
    aviao.location = (0, 5, 0)
    bpy.ops.transform.resize(value=(0.48, 0.48, 0.63))

    # Fechar o objeto criando uma esfera para fazer um contorno redondo
    bpy.ops.mesh.primitive_uv_sphere_add(size=2.85)
    bottom_1 = bpy.context.scene.objects.active
    bpy.ops.object.shade_smooth()
    bpy.ops.transform.resize(value=(0.48, 0.48, 0.20))
    bottom_1.location.z = 0
    bottom_1.location.y = 5

    # Parte 2 do aviao
    aviao_2 = shape_foguetao()
    aviao_2.location = (0, 4, 0)
    bpy.ops.transform.resize(value=(0.65, 0.65, 0.7))

    # Fechar o objeto criando uma esfera para fazer um contorno redondo
    bpy.ops.mesh.primitive_uv_sphere_add(size=2.85)
    bottom_2 = bpy.context.scene.objects.active
    bpy.ops.object.shade_smooth()
    bpy.ops.transform.resize(value=(0.65, 0.65, 0.20))
    bottom_2.location.z = 0
    bottom_2.location.y = 4

    # Retornar as duas partes juntas
    return join_objects([aviao, aviao_2, bottom_1, bottom_2])

# Criacao das asas laterias do aviao
def asa():

    mesh = bpy.data.meshes.new('mesh')  # create a new mesh "me" and name it to 'Swe-ep'
    # create a new object "ob", name it to 'Sweep' and conect it with the mesh "me"
    ob = bpy.data.objects.new('Asa', mesh)
    bpy.context.scene.objects.link(ob)  # activate the object in the contex

    vert = []
    faces = []

    # Criacao dos vertices necessarios do objeto
    zero = vert.append([0.0, 0.0, 0.0])
    um = vert.append([3.65, 0.0, 0.0])
    dois = vert.append([1.0, 0.0, 2.0])
    tres = vert.append([0.0, 0.0, 2.0])
    quatro = vert.append([0.0, 0.0, 4.5])

    # Criacao das faces do objeto
    faces.append([0, 1, 2, 3])
    faces.append([3, 2, 4])

    # Join the mesh named "me" with the list of vert and faces the middle list [] is for edges,
    mesh.from_pydata(vert, [], faces) # but not use at same time as faces for not having trouble    faces.append([0, 1, 2])

    # Free all objects from selection
    bpy.ops.object.select_all(action='DESELECT')

    # Select the basic object "objA"
    bpy.context.scene.objects.active = ob

    # Aplicao de modificadores para criar profundidada no objeto e criar arestas suaves
    bpy.ops.object.modifier_add(type='SOLIDIFY')
    bpy.ops.object.modifier_add(type='BEVEL')
    bpy.context.object.modifiers["Solidify"].thickness = 0.2
    bpy.context.object.modifiers["Bevel"].width = 0.1
    bpy.context.object.modifiers["Bevel"].segments = 16
    bpy.ops.object.modifier_apply(apply_as="DATA", modifier="Solidify")
    bpy.ops.object.modifier_apply(apply_as="DATA", modifier="Bevel")

    return ob

# Criacao da asa superior do aviao
def asa_topo():
    mesh = bpy.data.meshes.new('mesh')  # create a new mesh "me" and name it to 'Swe-ep'
    # create a new object "ob", name it to 'Sweep' and conect it with the mesh "me"
    ob = bpy.data.objects.new('Asa Topo', mesh)
    bpy.context.scene.objects.link(ob)  # activate the object in the contex

    vert = []
    faces = []

    # Criacao dos vertices necessarios do objeto
    zero = vert.append([0.0, 0.0, 0.0])
    um = vert.append([1.0, 0.0, 0.0])
    dois = vert.append([5, 0.0, 0.0])
    tres = vert.append([1.0, 0.0, 4.0])
    quatro = vert.append([0.0, 0.0, 4.0])

    cinco = vert.append([5, 0.0, 0.5])
    seis = vert.append([1.0, 0.0, 0.5])
    sete = vert.append([0.0, 0.0, 0.5])

    # Criacao das faces do objeto
    faces.append([0, 1, 6, 7])
    faces.append([1, 2, 5, 6])
    faces.append([7, 6, 3, 4])
    faces.append([6, 5, 3])

    # Join the mesh named "me" with the list of vert and faces the middle list [] is for edges,
    mesh.from_pydata(vert, [],
                     faces)  # but not use at same time as faces for not having trouble    faces.append([0, 1, 2])

    # Free all objects from selection
    bpy.ops.object.select_all(action='DESELECT')

    # Select the basic object "objA"
    bpy.context.scene.objects.active = ob

    # Aplicao de modificadores para criar profundidada no objeto e criar arestas suaves
    bpy.ops.object.modifier_add(type='SOLIDIFY')
    bpy.ops.object.modifier_add(type='BEVEL')
    bpy.context.object.modifiers["Solidify"].thickness = 0.3
    bpy.context.object.modifiers["Bevel"].width = 0.1
    bpy.context.object.modifiers["Bevel"].segments = 16
    bpy.ops.object.modifier_apply(apply_as="DATA", modifier="Solidify")
    bpy.ops.object.modifier_apply(apply_as="DATA", modifier="Bevel")

    return ob

# Criacao do tanque principal(maior)
def tanque_principal():

    # Criacao do objeto
    tanque = shape_foguetao()

    # Criacao de uma esfera para fechar o objeto cilindrico, tendo assim um curva na base
    bpy.ops.mesh.primitive_uv_sphere_add(size=2.85)
    bottom = bpy.context.scene.objects.active
    bpy.ops.object.shade_smooth()
    bottom.scale.z = 0.5
    bottom.location.z = 0

    # Juncao dos dois objeto tornado-se assim num so
    tanque_final = join_objects([tanque, bottom])

    # Criacao do material
    material = material_tanque()
    # Apliacao do materaial no objeto
    setMaterial(tanque_final, material)

    # Retornar o objeto criado
    return tanque_final

# Criacao do foguetao completo
def foguetao(com_tanque = True):

    #tanque_principal = shape_foguetao()
    tanque_p = tanque_principal()
    tanque_p.name = "Tanque Grande"

    aviao_foguetao = aviao()

    # Asas
    #  Material
    material_asa_detail = material_asa_detalhe()

    asa_l = asa()
    asa_l.scale = (1.5, 3, 4.5)
    asa_l.location = (1.5, 3.3, 0.5)
    asa_l.rotation_euler = (0, 0, radians(-15))

    asa_l_detalhe = asa()
    asa_l_detalhe.scale = (1.45, 1.0, 4.5)
    asa_l_detalhe.location = (1.65, 3.35, 0.53)
    asa_l_detalhe.rotation_euler = (0, 0, radians(-15))
    setMaterial(asa_l_detalhe, material_asa_detalhe())


    asa_r = asa()
    asa_r.scale = (-1.5, 3, 4.5)
    asa_r.location = (-1.5, 3.3, 0.5)
    asa_r.rotation_euler = (0, 0, radians(15))

    asa_r_detalhe = asa()
    asa_r_detalhe.scale = (-1.45, 1.0, 4.5)
    asa_r_detalhe.location = (-1.65, 3.35, 0.53)
    asa_r_detalhe.rotation_euler = (0, 0, radians(15))
    setMaterial(asa_r_detalhe, material_asa_detalhe())

    # Asa Topo
    asa_top = asa_topo()
    asa_top.location = (0, 6, 0.5)
    asa_top.rotation_euler = (0, radians(0), radians(90))

    # Tubos
    tube_size = 0.55
    tubo_1 = tubo()
    tubo_1.location = (0, 5.30, 0)
    tubo_1.rotation_euler = (radians(182), 0, 0)
    tubo_1.scale = (tube_size, tube_size, tube_size)
    setMaterial(tubo_1, material_asa_detalhe())

    tubo_2 = tubo()
    tubo_2.location = (0.80, 3.8, 0)
    tubo_2.rotation_euler = (radians(182), 0, 0)
    tubo_2.scale = (tube_size, tube_size, tube_size)
    setMaterial(tubo_2, material_asa_detalhe())

    tubo_3 = tubo()
    tubo_3.location = (-0.80, 3.8, 0)
    tubo_3.rotation_euler = (radians(182), 0, 0)
    tubo_3.scale = (tube_size, tube_size, tube_size)
    setMaterial(tubo_3, material_asa_detalhe())

    # Parte Baixo
    baixo = parte_baixo()


    lista_objectos_foguetao = [asa_l, asa_r, asa_top, baixo, aviao_foguetao]

    # Juntar todas as pecas do foguetao
    foguetao_completo = join_objects(lista_objectos_foguetao)
    foguetao_completo.name = "Foguetao"

    # Acessorios
    #  Simbolo da nasa
    simbolo = simbolo_nasa()
    simbolo.location = (-3, 3.52, 4.0)
    simbolo.rotation_euler = (radians(90), radians(-15), radians(15) + radians(180))

    # windows
    win = windows()

    if com_tanque:
        tanques_pequeno = propulsores()
    else:
        tanques_pequeno = add_empty((0, 0, 0))


    # Parenting the objects
    foguetao_parented = parent_objects(foguetao_completo, [simbolo, foguetao_completo, tanque_p, tanques_pequeno, asa_l_detalhe, asa_r_detalhe,
                                                           win, tubo_1, tubo_2, tubo_3])

    return foguetao_completo

# Criacao de um objeto tipo tubo de escape para sere utilizado na criacao dos tubos de escape do aviao do foguetao
def tubo():

    radius = 1
    bpy.ops.mesh.primitive_circle_add(radius=radius)
    tube = bpy.context.scene.objects.active

    # Edit Mode
    bpy.ops.object.editmode_toggle()

    # Create the cylindric part
    bpy.ops.mesh.extrude_region_move(TRANSFORM_OT_translate={"value": (0, 0, 1)})
    bpy.ops.transform.resize(value=(1.2, 1.2, 1.2))

    bpy.ops.mesh.extrude_region_move(TRANSFORM_OT_translate={"value": (0, 0, 1.5)})
    bpy.ops.transform.resize(value=(1.1, 1.1, 1.1))
    bpy.ops.object.editmode_toggle()

    # Modifiers
    bpy.ops.object.modifier_add(type='SOLIDIFY')
    bpy.ops.object.modifier_add(type='SUBSURF')
    bpy.context.object.modifiers["Solidify"].thickness = 0.30
    bpy.context.object.modifiers["Subsurf"].levels = 3

    bpy.ops.object.modifier_apply(apply_as="DATA", modifier="Solidify")
    bpy.ops.object.modifier_apply(apply_as="DATA", modifier="Subsurf")

    return tube

def parte_baixo():

    # Parte de baixo
    c = cube((1, 1, 1), (0, 4, 2))
    return c

# Simbolo da nasa colado no aviao
def simbolo_nasa():

    # Criar o material na cena do simbolo da nasa
    material_simbolo_nasa()

    # Pegar a referencia do material
    material = bpy.data.materials.get("Simbolo Nasa")

    # Criar um cilindro
    bpy.ops.mesh.primitive_cylinder_add()
    obj = bpy.context.scene.objects.active
    obj.name = "Simbolo Nasa"
    obj.scale = (1.15, 1.15, 0.01)

    # Aplicar o material ao objeto
    setMaterial(obj, material)

    return obj

# Janelas do aviao do foguetao
def windows():

    # Criacao do material das janelas
    mat = material_asa_detalhe()

    windows = []

    # Posicao no eixo Z das janelas
    z = 3
    # Criacao das janelas laterias no aviao da parte direita e esquerda , range(2)-> 0 e 1
    for j in range(2):

        # Posicao das janelas caso j = 0, janelas da esquerda
        if j == 0:
            location = (-1.1, 5.8, z)
            rotation = (0, radians(90), radians(-35))
        # Posicao das janelas caso j = 1, janelas da direita
        else:
            location = (1.1, 5.8, z)
            rotation = (0, radians(90), radians(35))

        # Criacao das janelas laterais com a posicao correta
        # "janelas_laterais" -> numero de janelas laterais a serem criadas
        janelas_laterais = 8
        for i in range(janelas_laterais):
            w = cylinder(size=(0.15, 0.15, 0.05))
            windows.append(w)
            w.location = location
            w.rotation_euler = rotation
            location = (location[0], location[1], location[2] + 2)

    # Criacao das janelas do cockpit
    w = cylinder(size=(0.15, 0.15, 0.05))
    w.location = (0.35, 6.12, 19.5)
    w.rotation_euler = (radians(105), 0, radians(-15))
    windows.append(w)

    # Criacao das janelas do cockpit
    w = cylinder(size=(0.15, 0.15, 0.05))
    w.location = (-0.35, 6.12, 19.5)
    w.rotation_euler = (radians(105), 0, radians(15))
    windows.append(w)
    # Criacao das janelas do cockpit
    w = cube(size=(0.28, 0.15, 0.05))
    w.location = (0.0, 5.95, 20.4)
    w.rotation_euler = (radians(110), 0, 0)
    windows.append(w)

    # Juntar todas as janelas num objeto
    windows = join_objects(windows)

    # Adicionar o material ao objeto
    setMaterial(windows, mat)

    # retornar a janelas todas juntas
    return windows

##################################################
###                OUTROS
##################################################

# Criacao do fumo do foguetao
# A animacao do fumo é criada usando a simulacao de fumo padrao do blender
def smoke(dois = True):

    # Criacao do emissor esquerdo do fumo
    emitter_left = cylinder(size=(1, 1, 0.25))
    emitter_left.name = "Emitter Left"
    bpy.ops.object.quick_smoke()
    emitter_left.modifiers["Smoke"].flow_settings.smoke_flow_type = 'BOTH'
    emitter_left.modifiers["Smoke"].flow_settings.subframes = 4
    emitter_left.modifiers["Smoke"].flow_settings.smoke_color = (1, 1, 1)

    if dois:
        emitter_left.location = (3.85, 0, -6.35)

        # Criacao do emissor direito do fumo
        # Free all objects from selection
        bpy.ops.object.select_all(action='DESELECT')
        emitter_right = cylinder(size=(1, 1, 0.25))
        emitter_right.name = "Emitter Right"
        bpy.data.objects["Emitter Right"].select = True
        emitter_right.location = (-3.85, 0, -6.35)

        bpy.ops.object.modifier_add(type="SMOKE")
        emitter_right.modifiers["Smoke"].smoke_type = "FLOW"
        emitter_right.modifiers["Smoke"].flow_settings.smoke_flow_type = 'BOTH'
        emitter_right.modifiers["Smoke"].flow_settings.subframes = 4
        emitter_right.modifiers["Smoke"].flow_settings.smoke_color = (1, 1, 1)

    else:
        emitter_left.location.z = -1
        emitter_right = add_empty((0, 0, 0))

    # Referencia ao domain
    domain = bpy.data.objects['Smoke Domain']
    domain.select = True
    domain.scale = (15, 15, 60)
    domain.location = (0, 0, -60.5)

    # Definicoes do domain
    domain.modifiers["Smoke"].domain_settings.resolution_max = 64
    domain.modifiers["Smoke"].domain_settings.dissolve_speed = 20
    domain.modifiers["Smoke"].domain_settings.amplify = 3
    domain.modifiers["Smoke"].domain_settings.burning_rate = 3
    domain.modifiers["Smoke"].domain_settings.flame_vorticity = 1
    domain.modifiers["Smoke"].domain_settings.flame_smoke = 8
    domain.modifiers["Smoke"].domain_settings.time_scale = 0.5
    domain.modifiers["Smoke"].domain_settings.use_dissolve_smoke = True
    domain.modifiers["Smoke"].domain_settings.use_adaptive_domain = True
    domain.modifiers["Smoke"].domain_settings.use_high_resolution = True


    # Criacao de vento para dar mais efeito ao vento
    bpy.ops.object.effector_add(type="WIND")
    wind = bpy.context.scene.objects.active
    wind.location = (3, 4, -1)
    wind.rotation_euler = (radians(180), 0, 0)
    wind.field.strength = 10
    wind.field.noise = 0.5


    return parent_objects(emitter_left, [emitter_left, emitter_right, domain, wind])


##################################################
###                CENAS
##################################################

# Cena: Lancamento do foguetao na Terra.
def cena_1():
    posx = -140
    posy = 100

    # Criacao do fundo
    scene = bpy.context.scene
    texture = bpy.data.textures.get("hdri")
    scene.world.active_texture = texture
    scene.world.use_sky_real = True

    # Cenario
    edi_princi = edificio_principal()
    mat_estru = material_estrutura_lancamento()
    mat_estru.diffuse_color = (0.3, 0.3, 0.3)
    setMaterial(edi_princi, mat_estru)
    parque_estacionamento()

    # Base de lancamento
    base_lancamento = base_lancamento_foguetao()
    mat_base = material_base_lancamento()
    setMaterial(base_lancamento, mat_base)
    edificio_pesquisa_operacoes()
    estru = estrutura()
    estru.location = (-139, 110, 60)
    estru.rotation_euler.z = radians(-17)

    # Criacao e aplicacao do material
    mat_estru = material_estrutura_lancamento()
    setMaterial(estru, mat_estru)

    # Chao de terra
    chao = cube(size=(2000, 2000, 1))

    mat_chao = material_chao()
    setMaterial(chao, mat_chao)

    # Foguetao
    fogue = foguetao(com_tanque=False)
    fogue.location = (-158, 118.6, 21)
    fogue.rotation_euler.z = radians(-17)

    # Tanques pequenos separados para depois animar a parte
    tanques_pequeno = propulsores()

    # Iuminacao da cena
    bpy.ops.object.lamp_add(type="SUN")
    light_1 = bpy.context.scene.objects.active
    light_1.name = "Sun"
    light_1.rotation_euler = (radians(239), radians(-226), radians(-71))
    light_1.data.shadow_ray_samples = 2
    light_1.data.shadow_method = "RAY_SHADOW"
    light_1.data.shadow_color = (0.16, 0.16, 0.16)

    # Criacao do fumo
    smoke_effect = smoke()

    # Parenting the foguetao to the particles
    tanques_fumo = parent_objects(tanques_pequeno, [tanques_pequeno, smoke_effect])
    tanques_fumo.location = (-163.3, 114.73, 45.4)
    tanques_fumo.rotation_euler.z = radians(-17)

    # Animacao
    # Definir o numero de frames da cena
    anim = bpy.context.scene
    anim.frame_start = 1
    anim.frame_end = 500

    # Animacao foguetao
    fogue.keyframe_insert(data_path='location', frame=220, index=-1)
    tanques_fumo.keyframe_insert(data_path='location', frame=220, index=-1)

    fogue.location.z += 250
    tanques_fumo.location.z += 250

    fogue.keyframe_insert(data_path='location', frame=300, index=-1)
    tanques_fumo.keyframe_insert(data_path='location', frame=300, index=-1)

    fogue.location.z += 200
    tanques_fumo.location.z += 200

    fogue.keyframe_insert(data_path='location', frame=369, index=-1)
    tanques_fumo.keyframe_insert(data_path='location', frame=369, index=-1)
    tanques_fumo.keyframe_insert(data_path='rotation_euler', frame=369, index=-1)

    fogue.location.z += 800
    tanques_fumo.location.z -= 800
    tanques_fumo.location.y -= 150
    tanques_fumo.rotation_euler.z += radians(120)
    tanques_fumo.rotation_euler.y += radians(-268)

    fogue.keyframe_insert(data_path='location', frame=500, index=-1)
    tanques_fumo.keyframe_insert(data_path='location', frame=500, index=-1)
    tanques_fumo.keyframe_insert(data_path='rotation_euler', frame=500, index=-1)

    # Interpolate the keys!!!
    # Isto foi usado para nao causar uma descrepancia na posicao do foguetao e do propulsores, caso contrario a animacao
    # destes nao coincida.Isto porque o blender cria uma curva de animacao e como nao criamos a animacao por python nao a podemos alterar
    # pelo menos facilmente
    for fc in fogue.animation_data.action.fcurves:
        #fc.extrapolation = 'LINEAR'
        for kp in fc.keyframe_points:
            kp.interpolation = 'LINEAR'

    for fc in tanques_fumo.animation_data.action.fcurves:
        #fc.extrapolation = 'LINEAR'
        for kp in fc.keyframe_points:
            kp.interpolation = 'LINEAR'


    # Animacao Fumo
    emitter_left = bpy.data.objects['Emitter Left']
    emitter_right = bpy.data.objects['Emitter Right']

    emitter_left.modifiers["Smoke"].flow_settings.surface_distance = 0
    emitter_right.modifiers["Smoke"].flow_settings.surface_distance = 0
    emitter_left.keyframe_insert(data_path='modifiers["Smoke"].flow_settings.surface_distance', frame=0, index=-1)
    emitter_right.keyframe_insert(data_path='modifiers["Smoke"].flow_settings.surface_distance', frame=0, index=-1)
    emitter_left.modifiers["Smoke"].flow_settings.surface_distance = 0
    emitter_right.modifiers["Smoke"].flow_settings.surface_distance = 0
    emitter_left.keyframe_insert(data_path='modifiers["Smoke"].flow_settings.surface_distance', frame=200, index=-1)
    emitter_right.keyframe_insert(data_path='modifiers["Smoke"].flow_settings.surface_distance', frame=200, index=-1)

    emitter_left.modifiers["Smoke"].flow_settings.surface_distance = 1.5
    emitter_right.modifiers["Smoke"].flow_settings.surface_distance = 1.5
    emitter_left.keyframe_insert(data_path='modifiers["Smoke"].flow_settings.surface_distance', frame=220, index=-1)
    emitter_right.keyframe_insert(data_path='modifiers["Smoke"].flow_settings.surface_distance', frame=220, index=-1)

    emitter_left.keyframe_insert(data_path='modifiers["Smoke"].flow_settings.surface_distance', frame=369, index=-1)
    emitter_right.keyframe_insert(data_path='modifiers["Smoke"].flow_settings.surface_distance', frame=369, index=-1)

    emitter_left.modifiers["Smoke"].flow_settings.surface_distance = 0
    emitter_right.modifiers["Smoke"].flow_settings.surface_distance = 0
    emitter_left.keyframe_insert(data_path='modifiers["Smoke"].flow_settings.surface_distance', frame=370, index=-1)
    emitter_right.keyframe_insert(data_path='modifiers["Smoke"].flow_settings.surface_distance', frame=370, index=-1)

    # Animacao da camera
    cam = add_cam((374, 310, 201), (radians(70.4), radians(1), radians(122.8)))
    cam.data.clip_end = 2000
    cam.keyframe_insert(data_path='location', frame=1, index=-1)
    cam.keyframe_insert(data_path='rotation_euler', frame=1, index=-1)

    cam.location = (24, 288, 101)
    cam.rotation_euler = (radians(71), radians(0.6), radians(137))

    cam.keyframe_insert(data_path='location', frame=100, index=-1)
    cam.keyframe_insert(data_path='rotation_euler', frame=100, index=-1)

    cam.location = (-200, 235, 17)
    cam.rotation_euler = (radians(100), radians(0.6), radians(197.6))

    cam.keyframe_insert(data_path='location', frame=200, index=-1)
    cam.keyframe_insert(data_path='rotation_euler', frame=200, index=-1)

    cam.keyframe_insert(data_path='location', frame=221, index=-1)
    cam.keyframe_insert(data_path='rotation_euler', frame=221, index=-1)

    cam.rotation_euler = (radians(152.3), radians(0.6), radians(200))
    cam.keyframe_insert(data_path='rotation_euler', frame=300, index=-1)
    cam.keyframe_insert(data_path='location', frame=300, index=-1)

    cam.rotation_euler = (radians(165.3), radians(0.6), radians(200))
    cam.location.z += 200
    cam.keyframe_insert(data_path='rotation_euler', frame=500, index=-1)
    cam.keyframe_insert(data_path='location', frame=500, index=-1)

# Cena: Foguetao a sair da Terra vista do espaco
def cena_2():

    # Criacao do fundo
    scene = bpy.context.scene
    texture = bpy.data.textures.get("stars")
    scene.world.active_texture = texture
    scene.world.use_sky_real = True

    # Criacao do planeta terra
    bpy.ops.mesh.primitive_uv_sphere_add(size=10, ring_count=64, calc_uvs=True)
    planeta = bpy.context.scene.objects.active
    planeta.rotation_euler = (radians(95), radians(12), radians(-270))
    planeta.name = "Terra"
    bpy.ops.object.shade_smooth()

    # Criacao e apliacao do material
    material_terra = material_planeta_terra()
    setMaterial(planeta, material_terra)

    # Criacao das nuvens
    # Foi criado uma esfera com um tamanho ligeiramente superior ao da terra, para ficar sobreposta, sendo assim visivel
    bpy.ops.mesh.primitive_uv_sphere_add(size=10.05, ring_count=64, calc_uvs=True)
    nuvens = bpy.context.scene.objects.active
    nuvens.rotation_euler = (radians(-48), radians(4), radians(38))
    nuvens.name = "Nuvens"
    bpy.ops.object.shade_smooth()

    # Criacao e aplicacao do material
    material_nuvens = material_planeta_terra_nuvens()
    setMaterial(nuvens, material_nuvens)

    # Iuminacao da cena
    bpy.ops.object.lamp_add(type="SUN")
    light_1 = bpy.context.scene.objects.active
    light_1.name = "Sun"
    light_1.rotation_euler = (radians(250), radians(-225), radians(1))
    light_1.data.shadow_ray_samples = 2

    # Criacao da camera
    cam = add_cam((4, 20, 2), (radians(85), 0, radians(150)))

    # Criacao do foguetao
    fogue = foguetao(com_tanque=False)

    # Criacao do fumo
    smoke_effect = smoke(dois=False)
    smoke_effect.location = (-0.0, 4.5, -2.0)

    # Parenting the foguetao to the particles
    fogue = parent_objects(fogue, [fogue, smoke_effect])

    # Definicao do numero de frame da cena
    anim = bpy.context.scene
    anim.frame_start = 1
    anim.frame_end = 300

    # Animacao Foguetao e da camera
    fogue.location = (-0.87, 9.9, 1.32)
    fogue.scale = (0.002, 0.002, 0.002)
    fogue.rotation_euler = (radians(-121), radians(-130), radians(35))
    fogue.keyframe_insert(data_path='location', frame=1, index=-1)
    fogue.keyframe_insert(data_path='scale', frame=1, index=-1)
    fogue.keyframe_insert(data_path='rotation_euler', frame=1, index=-1)
    cam.keyframe_insert(data_path='rotation_euler', frame=1, index=-1)


    fogue.location = (-2.70353, 20.07038, 4.96291)
    fogue.scale = (0.1, 0.1, 0.1)
    cam.rotation_euler = (radians(98.6), 0, radians(133.3))


    cam.keyframe_insert(data_path='rotation_euler', frame=270, index=-1)
    fogue.keyframe_insert(data_path='location', frame=300, index=-1)
    fogue.keyframe_insert(data_path='scale', frame=300, index=-1)
    fogue.keyframe_insert(data_path='rotation_euler', frame=300, index=-1)

# Cena: Marte visto do espaco
def cena_3():

    # Criacao do fundo
    scene = bpy.context.scene
    texture = bpy.data.textures.get("stars")
    scene.world.active_texture = texture
    scene.world.use_sky_real = True

    # Criacao do planeta Marte
    bpy.ops.mesh.primitive_uv_sphere_add(size=10, ring_count=64, calc_uvs=True)
    marte = bpy.context.scene.objects.active
    marte.location = (0, 0, 0)
    marte.rotation_euler = (radians(65), radians(-33), radians(0))
    marte.name = "Marte"
    bpy.ops.object.shade_smooth()
    # Criacao do material e aplicao no respetivo objeto
    mat_marte = material_marte()
    setMaterial(marte, mat_marte)

    # Iuminacao da cena
    bpy.ops.object.lamp_add(type="SUN")
    light_1 = bpy.context.scene.objects.active
    light_1.name = "Sun"
    light_1.rotation_euler = (radians(288), radians(-220), radians(1))
    light_1.data.shadow_ray_samples = 2

    # Foguetao
    bpy.ops.object.select_all(action='DESELECT')
    fogue = foguetao(com_tanque=False)
    fogue.scale = (0.05, 0.05, 0.05)

    # Elimiar o tanque grande, nao é necessario
    # Deselect all object
    bpy.ops.object.select_all(action='DESELECT')
    tanque = bpy.data.objects['Tanque Grande']
    tanque.select = True
    bpy.ops.object.delete()
    tanque = tanque_principal()
    tanque.scale = (tanque.scale.x * 0.05, tanque.scale.y * 0.05, tanque.scale.z * 0.05)

    # Criacao do fo
    smoke_effect = smoke(dois=False)
    emitter_left = bpy.data.objects['Emitter Left']
    smoke_effect.location = (0.0, 4.03, 2.81)
    smoke_effect.scale = (0.022, 0.022, 0.014)

    # Parenting the foguetao to the particles
    fogue = parent_objects(fogue, [fogue, smoke_effect])


    # Animacao Foguetao
    fogue.location = (100, 0, 0)
    tanque.location = (100.2, 0, -0.23)
    fogue.rotation_euler = (radians(90), 0, radians(-90))
    tanque.rotation_euler = (radians(90), 0, radians(-90))

    fogue.keyframe_insert(data_path='location', frame=1, index=-1)
    tanque.keyframe_insert(data_path='location', frame=1, index=-1)

    fogue.location = (50, 0, 0)
    tanque.location = (50.2, 0, -0.23)

    fogue.keyframe_insert(data_path='location', frame=92, index=-1)
    tanque.keyframe_insert(data_path='location', frame=92, index=-1)
    tanque.keyframe_insert(data_path='rotation_euler', frame=92, index=-1)

    tanque.location = (63, 0, -3)
    tanque.rotation_euler = (radians(-80), 0, radians(40))

    tanque.keyframe_insert(data_path='location', frame=150, index=-1)
    tanque.keyframe_insert(data_path='rotation_euler', frame=150, index=-1)

    fogue.keyframe_insert(data_path='location', frame=92, index=-1)
    fogue.keyframe_insert(data_path='scale', frame=92, index=-1)
    fogue.location = (10, 0, 0)
    fogue.scale = (0.5 * fogue.scale.x, 0.5 * fogue.scale.y, 0.5 * fogue.scale.z)
    fogue.keyframe_insert(data_path='scale', frame=235, index=-1)
    fogue.keyframe_insert(data_path='location', frame=235, index=-1)


    # Interpolate the keys!!!
    # Isto foi usado para nao causar uma descrepancia na posicao do foguetao e do propulsores, caso contrario a animacao
    # destes nao coincida.Isto porque o blender cria uma curva de animacao e como nao criamos a animacao por python nao a podemos alterar
    # pelo menos facilmente
    for fc in fogue.animation_data.action.fcurves:
        #fc.extrapolation = 'LINEAR'
        for kp in fc.keyframe_points:
            kp.interpolation = 'LINEAR'

    for fc in tanque.animation_data.action.fcurves:
        #fc.extrapolation = 'LINEAR'
        for kp in fc.keyframe_points:
            kp.interpolation = 'LINEAR'

    # Criacao da camera
    cam = add_cam((90.8, 0.7, 0.2), (radians(87.3), radians(1), radians(100)))
    # Animacao da camera
    cam.keyframe_insert(data_path='location', frame=1, index=-1)
    cam.keyframe_insert(data_path='location', frame=79, index=-1)
    cam.keyframe_insert(data_path='rotation_euler', frame=79, index=-1)

    cam.location = (56, 3.3, 1.2)
    cam.rotation_euler = (radians(81), radians(1.2), radians(112.8))

    cam.keyframe_insert(data_path='location', frame=80, index=-1)
    cam.keyframe_insert(data_path='rotation_euler', frame=80, index=-1)

    cam.keyframe_insert(data_path='location', frame=129, index=-1)
    cam.keyframe_insert(data_path='rotation_euler', frame=129, index=-1)

    cam.location = (45.3, 15, 0.6)
    cam.rotation_euler = (radians(89.6), radians(1.1), radians(123.1))

    cam.keyframe_insert(data_path='location', frame=130, index=-1)
    cam.keyframe_insert(data_path='rotation_euler', frame=130, index=-1)

    # Animacao do fumo
    emitter_left.keyframe_insert(data_path='modifiers["Smoke"].flow_settings.surface_distance', frame=90, index=-1)
    emitter_left.modifiers["Smoke"].flow_settings.surface_distance = 0
    emitter_left.keyframe_insert(data_path='modifiers["Smoke"].flow_settings.surface_distance', frame=105, index=-1)


##################################################
###                MAIN PROGRAM
##################################################
# Para utilizar cada cena é necessario chamar a sua funcao correspondente ex: "cena_1", "cena_2"
# ATENCAO! como estamos a criar os objetos via python as suas texturas, caso o objeto tenha nao
# ficam memorizadas, logo nos tivemos que as alterar posteriormente, depois de as funcoes cena serem chamadas.
# Para alem disso, cada funcao cena esta associdada uma ficheiro .blend, ou seja uma cena no blender por motivos de simplificacao
# e tambem porque utilizamos configuracoes diferentes em cada cena , nomeadamente a textura do mundo, etc


# Eliminacao dos objetos posteriormente criados na cena.
delete_all()

# Chama da cena
cena_1()

